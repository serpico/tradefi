import numpy as np
import skimage

def get_objl_from_imap_and_classes(dset):
    for key, value in dset.items():
        objl = []
        for i in range(len(value['classes'])):
            indices = np.nonzero(value['instance_map']==i+1)
            x = np.mean(indices[1])
            y = np.mean(indices[0])
            obj = {
                'x': np.round(x),
                'y': np.round(y),
                'class': value['classes'][i],
                'object_id': i
            }

            objl.append(obj)
        value['object_list'] = objl


def get_objl_from_boxes_and_classes(dset):
    for key, value in dset.items():
        objl = []
        for i in range(len(value['classes'])):
            y1 = value['boxes'][i, 0]
            x1 = value['boxes'][i, 1]
            y2 = value['boxes'][i, 2]
            x2 = value['boxes'][i, 3]
            x = np.mean([x1, x2])
            y = np.mean([y1, y2])
            obj = {
                'x': np.round(x),
                'y': np.round(y),
                'class': value['classes'][i],
                'object_id': i
            }

            objl.append(obj)
        value['object_list'] = objl


def get_objl_from_classmap(dset):
    for key, value in dset.items():
        label_list = np.unique(value['class_map'])
        objl = []
        for label in label_list[1:]:  # don't consider class '0' which is background
            mask = value['class_map'] == label
            imap = skimage.measure.label(mask)
            region_list = skimage.measure.regionprops(imap)

            for region in region_list:
                x = region['centroid'][1]
                y = region['centroid'][0]

                obj = {
                    'x': np.round(x),
                    'y': np.round(y),
                    'class': label
                }
                objl.append(obj)
        value['object_list'] = objl