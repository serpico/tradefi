import numpy as np
import matplotlib.pyplot as plt
from . import tools
from copy import deepcopy


def norm_img_for_plot(img):
    k1 = 3
    k2 = 20
    img_norm = np.zeros(img.shape)
    if img.ndim == 2:
        img_norm = norm_img_channel_for_plot(img, k1, k2)
    elif img.ndim == 3:
        for c in range(img.shape[-1]):
            img_norm[:, :, c] = norm_img_channel_for_plot(img[:, :, c], k1, k2)
    return img_norm

    # img_norm = np.zeros(img.shape)
    # for c in range(img.shape[-1]):
    #     img_c = deepcopy(img[:, :, c])
    #     mu = np.mean(img_c)
    #     sig = np.std(img_c)
    #     val_min = mu - k1*sig
    #     val_max = mu + k2*sig
    #     # Saturate values:
    #     img_c[img_c <= val_min] = val_min
    #     img_c[img_c >= val_max] = val_max
    #     # Normalize [val_min, val_max] -> [0, 1]:
    #     a = 0
    #     b = 1
    #     img_c = a + (b - a) * (img_c - val_min) / (val_max - val_min)
    #     img_norm[:, :, c] = img_c
    # return img_norm


def norm_img_channel_for_plot(img, k1, k2):
    img_c = deepcopy(img)
    mu = np.mean(img_c)
    sig = np.std(img_c)
    val_min = mu - k1 * sig
    val_max = mu + k2 * sig
    # Saturate values:
    img_c[img_c <= val_min] = val_min
    img_c[img_c >= val_max] = val_max
    # Normalize [val_min, val_max] -> [0, 1]:
    a = 0
    b = 1
    img_c = a + (b - a) * (img_c - val_min) / (val_max - val_min)

    return img_c


def plot_scores_wrt_dist(dist_thr_list, pre_list, rec_list, f1s_list):
    # Prepare arrays for plot:
    label_list = list(f1s_list[0].keys())
    del label_list[0]  # delete label '0' (background)
    pre_array = np.zeros((len(dist_thr_list), len(label_list)))
    rec_array = np.zeros((len(dist_thr_list), len(label_list)))
    f1s_array = np.zeros((len(dist_thr_list), len(label_list)))
    for d in range(len(dist_thr_list)):
        for lbl_idx, lbl in enumerate(label_list):
            pre = pre_list[d][lbl]
            rec = rec_list[d][lbl]
            f1s = f1s_list[d][lbl]
            if pre == 'None': pre = 0
            if rec == 'None': rec = 0
            if f1s == 'None': f1s = 0

            pre_array[d, lbl_idx] = pre
            rec_array[d, lbl_idx] = rec
            f1s_array[d, lbl_idx] = f1s

    # Plot:
    fig, axes = plt.subplots(1, 3)
    fig.set_size_inches(15, 4)

    axes[0].plot(dist_thr_list, pre_array)
    axes[0].set_title('Precision')
    axes[0].set_xlabel('Tolerated position error [pixels]')
    axes[0].set_ylabel('Precision')
    axes[0].set_ylim(0, 1)
    axes[0].grid()

    axes[1].plot(dist_thr_list, rec_array)
    axes[1].set_title('Recall')
    axes[1].set_xlabel('Tolerated position error [pixels]')
    axes[1].set_ylabel('Recall')
    axes[1].set_ylim(0, 1)
    axes[1].grid()

    axes[2].plot(dist_thr_list, f1s_array)
    axes[2].set_title('F1-score')
    axes[2].set_xlabel('Tolerated position error [pixels]')
    axes[2].set_ylabel('F1-score')
    axes[2].set_ylim(0, 1)
    axes[2].grid()
    axes[2].legend(labels=label_list)

    plt.close(fig)

    return fig

def plot_prediction_segmentation(data, targ, pred):
    # Prepare arrays for plot:
    img_pred = ( pred ).astype(dtype=int)
    img_targ = ( targ ).astype(dtype=int)
    img_data = ( tools.norm_rgb_img_for_plot(data) ).astype(dtype=float)
    
    # Plot:
    fontsize = 10
    fig, axes = plt.subplots(1,3)
    fig.set_size_inches(20,10)

    axes[0].imshow(img_data, vmin=0, vmax=1)
    axes[0].set_title('Data', fontsize=fontsize)
    
    axes[1].imshow(img_pred, vmin=np.min(img_pred), vmax=np.max(img_pred))
    axes[1].set_title(f'Prediction', fontsize=fontsize)

    axes[2].imshow(img_targ, vmin=np.min(img_targ), vmax=np.max(img_targ))
    axes[2].set_title('Target', fontsize=fontsize)
    
    plt.close(fig)
    
    return fig
    
    
def plot_prediction_autoencoder(data, targ, pred):
    # Prepare arrays for plot:
    img_pred = ( tools.norm_rgb_img_for_plot(pred) ).astype(dtype=float)
    img_targ = ( tools.norm_rgb_img_for_plot(targ) ).astype(dtype=float)
    img_data = ( tools.norm_rgb_img_for_plot(data) ).astype(dtype=float)
    
    # Plot:
    fontsize = 10
    fig, axes = plt.subplots(1,3)
    fig.set_size_inches(20,10)

    axes[0].imshow(img_data)
    axes[0].set_title('Data', fontsize=fontsize)
    
    axes[1].imshow(img_pred)
    axes[1].set_title(f'Prediction', fontsize=fontsize)

    axes[2].imshow(img_targ)
    axes[2].set_title('Target', fontsize=fontsize)
    
    plt.close(fig)
    
    return fig