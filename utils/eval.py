import numpy as np
from sklearn.metrics import pairwise_distances
from pycm import ConfusionMatrix


class Evaluator:

    def __init__(self, dset_true, dset_pred):
        if dset_true.keys() != dset_pred.keys():
            raise Exception('Data sets do not have the same keys')
        self.dset_true = dset_true
        self.dset_pred = dset_pred

        self.dmat_dict = {}

    def get_distance_matrix(self):
        for key in self.dset_true.keys():
            # Prepare data points for pairwise_distances:
            objl_true = self.dset_true[key]['object_list']
            objl_pred = self.dset_pred[key]['object_list']

            coords_true = np.zeros((len(objl_true), 2))
            coords_pred = np.zeros((len(objl_pred), 2))
            for idx, obj in enumerate(objl_true):
                coords_true[idx, 0] = obj['y']
                coords_true[idx, 1] = obj['x']
            for idx, obj in enumerate(objl_pred):
                coords_pred[idx, 0] = obj['y']
                coords_pred[idx, 1] = obj['x']

            # Compute pairwise distances:
            if coords_pred.shape[0] is not 0:  # only compute dmat if something has been detected
                self.dmat_dict[key] = pairwise_distances(coords_true, coords_pred, metric='euclidean')
            else:  # if no detections then dmat is not defined (and later on all scores are 0)
                self.dmat_dict[key] = None

    def get_cm(self, dist_thr):
        self.get_distance_matrix()

        z_true_global = []
        z_pred_global = []
        n_multi_hit = 0
        for key, dmat in self.dmat_dict.items():
            y_true = [obj['class'] for obj in self.dset_true[key]['object_list']]
            y_pred = [obj['class'] for obj in self.dset_pred[key]['object_list']]

            if dmat is not None:  # i.e., if something has been detected
                # y_true and y_pred are not of same lenght. Below I transform y_pred into z_pred and y_true into z_true,
                # where z_true and z_pred have 1 to 1 correspondence between their elements:
                z_pred = [None for _ in range(len(y_true))]

                # Correspondence matrix where '1' means that an entry from pred is situated at a distance <=dist_thr
                # to an entry of true:
                dmat = dmat <= dist_thr

                match_idx_list = []  # this list stores all the entries in y_pred that have a match in y_true
                for idx in range(len(y_true)):
                    indices = np.nonzero(dmat[idx, :])[0]
                    if len(indices) == 1:  # only 1 correspondence = true positive // this is necessary in case 1 true matches several pred
                        z_pred[idx] = y_pred[indices[0]]
                        match_idx_list.append(indices[0])
                    elif len(indices) > 1:  # multiple correspondences (not counted as tp)
                        z_pred[idx] = 0
                        n_multi_hit += 1
                        print(f'multi hit: {len(indices)}')
                    elif len(indices) < 1:  # no correspondence = false negative
                        z_pred[idx] = 0
                    else:
                        print(f'Exception!! {len(indices)}')

                # At this point, z_pred has a 1-to-1 corresp to y_true. But we also have to take into account the
                # predictions that have no correspondence in GT, because those are false positives. In z_true, these
                # correspond to the negative class (lbl=0)
                y_pred_no_corresp = y_pred

                match_idx_list = np.unique(match_idx_list)  # this is necessary in case 1 pred matches several true. Therefore 1 pred is counted only once as true positive
                match_idx_list = np.flip(np.sort(match_idx_list))  # sort in descending order, else it is a mess deleting elements y idx
                for idx in match_idx_list:
                    del y_pred_no_corresp[idx]
                y_true_no_corresp = [0 for _ in range(len(y_pred_no_corresp))]  # give label 0

                z_true = y_true + y_true_no_corresp
                z_pred = z_pred + y_pred_no_corresp

            else:  # if nothing has been detected
                z_true = y_true
                z_pred = [0 for _ in range(len(z_true))]

            z_true_global += z_true
            z_pred_global += z_pred

            # print(y_found)

        cm = ConfusionMatrix(actual_vector=z_true_global, predict_vector=z_pred_global)
        return cm

    def get_cm_wrt_dist(self, dist_thr_list):
        cm_list = []
        for dist_thr in dist_thr_list:
            cm = self.get_cm(dist_thr)
            cm_list.append(cm)

        return cm_list

    def get_scores_wrt_dist(self, dist_thr_list):
        cm_list = self.get_cm_wrt_dist(dist_thr_list)
        pre_list = []
        rec_list = []
        f1s_list = []
        for cm in cm_list:
            pre_list.append(cm.PPV)
            rec_list.append(cm.TPR)
            f1s_list.append(cm.F1)

        return pre_list, rec_list, f1s_list, cm_list


class Evaluator_old3:

    def __init__(self, dset_true, dset_pred):
        if dset_true.keys() != dset_pred.keys():
            raise Exception('Data sets do not have the same keys')
        self.dset_true = dset_true
        self.dset_pred = dset_pred

        self.dmat_dict = {}

    def get_distance_matrix(self):
        for key in self.dset_true.keys():
            # Prepare data points for pairwise_distances:
            objl_true = self.dset_true[key]['object_list']
            objl_pred = self.dset_pred[key]['object_list']

            coords_true = np.zeros((len(objl_true), 2))
            coords_pred = np.zeros((len(objl_pred), 2))
            for idx, obj in enumerate(objl_true):
                coords_true[idx, 0] = obj['y']
                coords_true[idx, 1] = obj['x']
            for idx, obj in enumerate(objl_pred):
                coords_pred[idx, 0] = obj['y']
                coords_pred[idx, 1] = obj['x']

            # Compute pairwise distances:
            self.dmat_dict[key] = pairwise_distances(coords_true, coords_pred, metric='euclidean')

    def get_cm(self, dist_thr):
        self.get_distance_matrix()

        z_true_global = []
        z_pred_global = []
        n_multi_hit = 0
        for key, dmat in self.dmat_dict.items():
            y_true = [obj['class'] for obj in self.dset_true[key]['object_list']]
            y_pred = [obj['class'] for obj in self.dset_pred[key]['object_list']]

            # y_true and y_pred are not of same lenght. Below I transform y_pred into z_pred and y_true into z_true,
            # where z_true and z_pred have 1 to 1 correspondence between their elements:
            z_pred = [None for _ in range(len(y_true))]

            # Correspondence matrix where '1' means that an entry from pred is situated at a distance <=dist_thr
            # to an entry of true:
            dmat = dmat <= dist_thr

            match_idx_list = []  # this list stores all the entries in y_pred that have a match in y_true
            for idx in range(len(y_true)):
                indices = np.nonzero(dmat[idx, :])[0]
                if len(indices) == 1:  # only 1 correspondence = true positive // this is necessary in case 1 true matches several pred
                    z_pred[idx] = y_pred[indices[0]]
                    match_idx_list.append(indices[0])
                elif len(indices) > 1:  # multiple correspondences (not counted as tp)
                    z_pred[idx] = 0
                    n_multi_hit += 1
                    print(f'multi hit: {len(indices)}')
                elif len(indices) < 1:  # no correspondence = false negative
                    z_pred[idx] = 0
                else:
                    print(f'Exception!! {len(indices)}')

            # At this point, z_pred has a 1-to-1 corresp to y_true. But we also have to take into account the
            # predictions that have no correspondence in GT, because those are false positives. In z_true, these
            # correspond to the negative class (lbl=0)
            y_pred_no_corresp = y_pred
            
            match_idx_list = np.unique(match_idx_list)  # this is necessary in case 1 pred matches several true. Therefore 1 pred is counted only once as true positive
            match_idx_list = np.flip(np.sort(match_idx_list))  # sort in descending order, else it is a mess deleting elements y idx
            for idx in match_idx_list:
                del y_pred_no_corresp[idx]
            y_true_no_corresp = [0 for _ in range(len(y_pred_no_corresp))]  # give label 0

            z_true = y_true + y_true_no_corresp
            z_pred = z_pred + y_pred_no_corresp

            z_true_global += z_true
            z_pred_global += z_pred

            # print(y_found)

        cm = ConfusionMatrix(actual_vector=z_true_global, predict_vector=z_pred_global)
        return cm

    def get_cm_wrt_dist(self, dist_thr_list):
        cm_list = []
        for dist_thr in dist_thr_list:
            cm = self.get_cm(dist_thr)
            cm_list.append(cm)

        return cm_list

    def get_scores_wrt_dist(self, dist_thr_list):
        cm_list = self.get_cm_wrt_dist(dist_thr_list)
        pre_list = []
        rec_list = []
        f1s_list = []
        for cm in cm_list:
            pre_list.append(cm.PPV)
            rec_list.append(cm.TPR)
            f1s_list.append(cm.F1)

        return pre_list, rec_list, f1s_list


class Evaluator_old2:

    def __init__(self, dset_true, dset_pred):
        if dset_true.keys() != dset_pred.keys():
            raise Exception('Data sets do not have the same keys')
        self.dset_true = dset_true
        self.dset_pred = dset_pred

        self.dmat_dict = {}

    def get_distance_matrix(self):
        for key in self.dset_true.keys():
            # Prepare data points for pairwise_distances:
            objl_true = self.dset_true[key]['object_list']
            objl_pred = self.dset_pred[key]['object_list']

            coords_true = np.zeros((len(objl_true), 2))
            coords_pred = np.zeros((len(objl_pred), 2))
            for idx, obj in enumerate(objl_true):
                coords_true[idx, 0] = obj['y']
                coords_true[idx, 1] = obj['x']
            for idx, obj in enumerate(objl_pred):
                coords_pred[idx, 0] = obj['y']
                coords_pred[idx, 1] = obj['x']

            # Compute pairwise distances:
            self.dmat_dict[key] = pairwise_distances(coords_true, coords_pred, metric='euclidean')

    def get_cm(self, dist_thr):
        self.get_distance_matrix()

        y_true_global = []
        y_found_global = []
        n_multi_hit = 0
        for key, dmat in self.dmat_dict.items():
            y_true = [obj['class'] for obj in self.dset_true[key]['object_list']]
            y_pred = [obj['class'] for obj in self.dset_pred[key]['object_list']]

            # y_true and y_pred are not of same lenght. Below I transform y_pred into y_found, where y_found has 1 to 1
            # correspondence with y_true:
            y_found = [None for _ in range(len(y_true))]

            # Correspondence matrix where '1' means that an entry from pred is situated at a distance <=dist_thr
            # to an entry of true:
            dmat = dmat <= dist_thr

            for idx in range(len(y_true)):
                indices = np.nonzero(dmat[idx, :])[0]
                if len(indices) == 1:  # only 1 correspondence = true positive
                    y_found[idx] = y_pred[indices[0]]
                elif len(indices) > 1:  # multiple correspondences (not counted as tp)
                    y_found[idx] = 0
                    n_multi_hit += 1
                    print(f'multi hit: {len(indices)}')
                elif len(indices) < 1:  # no correspondence = false negative
                    y_found[idx] = 0
                else:
                    print(f'Exception!! {len(indices)}')

            y_true_global += y_true
            y_found_global += y_found

            #print(y_found)

        cm = ConfusionMatrix(actual_vector=y_true_global, predict_vector=y_found_global)
        return cm

    def get_cm_wrt_dist(self, dist_thr_list):
        cm_list = []
        for dist_thr in dist_thr_list:
            cm = self.get_cm(dist_thr)
            cm_list.append(cm)

        return cm_list

    def get_scores_wrt_dist(self, dist_thr_list):
        cm_list = self.get_cm_wrt_dist(dist_thr_list)
        pre_list = []
        rec_list = []
        f1s_list = []
        for cm in cm_list:
            pre_list.append(cm.PPV)
            rec_list.append(cm.TPR)
            f1s_list.append(cm.F1)

        return pre_list, rec_list, f1s_list



class Evaluator_old():

    def __init__(self, dset_true, dset_pred):
        if dset_true.keys() != dset_pred.keys():
            raise Exception('Data sets do not have the same keys')
        self.dset_true = dset_true
        self.dset_pred = dset_pred

        self.dmat_dict = {}

    def get_distance_matrix(self):
        for key in self.dset_true.keys():
            # Prepare data points for pairwise_distances:
            objl_true = self.dset_true[key]['object_list']
            objl_pred = self.dset_pred[key]['object_list']

            coords_true = np.zeros((len(objl_true), 2))
            coords_pred = np.zeros((len(objl_pred), 2))
            for idx, obj in enumerate(objl_true):
                coords_true[idx, 0] = obj['y']
                coords_true[idx, 1] = obj['x']
            for idx, obj in enumerate(objl_pred):
                coords_pred[idx, 0] = obj['y']
                coords_pred[idx, 1] = obj['x']

            # Compute pairwise distances:
            self.dmat_dict[key] = pairwise_distances(coords_true, coords_pred, metric='euclidean')

    def evaluate_dmat(self, dmat, dist_thr):
        n_true = dmat.shape[0]
        n_pred = dmat.shape[1]
        # 'matches' is a vector where elements represent GT boxes
        # the number in these elements is the number of matches among the predict boxes
        matches = np.sum(dmat >= dist_thr, axis=0)

        n_tp = np.sum(matches == 1)  # is TP if box_true has only 1 match
        n_multi_hit = np.sum(matches > 1)

        return n_true, n_pred, n_tp, n_multi_hit

    def evaluate_dmat_dict(self, dist_thr):
        n_true_global = 0
        n_pred_global = 0
        n_tp_global = 0
        n_multi_hit_global = 0
        for key, dmat in self.dmat_dict.items():
            n_true, n_pred, n_tp, n_multi_hit = self.evaluate_dmat(dmat, dist_thr)
            n_true_global += n_true
            n_pred_global += n_pred
            n_tp_global += n_tp
            n_multi_hit_global += n_multi_hit

        if n_multi_hit_global>0:
            print(f'{n_multi_hit_global} multi hits detected')

        return n_true_global, n_pred_global, n_tp_global, n_multi_hit_global

    def evaluate_wrt_dist(self, dist_thr_list):
        n_true_vec = np.zeros(len(dist_thr_list))
        n_pred_vec = np.zeros(len(dist_thr_list))
        n_tp_vec = np.zeros(len(dist_thr_list))
        n_multihit_vec = np.zeros(len(dist_thr_list))
        for idx, dist_thr in enumerate(dist_thr_list):
            n_true, n_pred, n_tp, n_multi_hit = self.evaluate_dmat_dict(dist_thr)

            n_true_vec[idx] = n_true
            n_pred_vec[idx] = n_pred
            n_tp_vec[idx] = n_tp
            n_multihit_vec[idx] = n_multi_hit

        return n_true_vec, n_pred_vec, n_tp_vec, n_multihit_vec

    def get_scores_from_tp_arrays(self, n_true_array, n_pred_array, n_tp_array):
        pre_array = n_tp_array / n_pred_array
        rec_array = n_tp_array / n_true_array
        f1_array = 2 * (pre_array * rec_array) / (pre_array + rec_array)

        # In case there are divisions by 0 (replace Nan by 0)
        pre_array[np.nonzero(np.isnan(pre_array))] = 0
        rec_array[np.nonzero(np.isnan(rec_array))] = 0
        f1_array[np.nonzero(np.isnan(f1_array))] = 0

        return pre_array, rec_array, f1_array

    def get_scores_wrt_dist(self, dist_thr_list):
        self.get_distance_matrix()
        n_true_vec, n_pred_vec, n_tp_vec, n_multihit_vec = self.evaluate_wrt_dist(dist_thr_list)
        pre_vec, rec_vec, f1_vec = self.get_scores_from_tp_arrays(n_true_vec, n_pred_vec, n_tp_vec)

        return pre_vec, rec_vec, f1_vec

