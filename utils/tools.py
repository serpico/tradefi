import numpy as np


def norm_ab(data, a, b):
    return a+(b-a)*(data-np.min(data)) / (np.max(data)-np.min(data))


# def norm_rgb_img_for_plot(img):
#     if img.ndim==2:
#         img = norm_ab(img, 0, 1)
#     elif img.ndim==3:
#         for idx in range(img.shape[-1]):
#             img[:,:,idx] = norm_ab(img[:,:,idx], 0, 1)
#     return img


def get_mean_and_std_from_dataset(data_list):
    mu_list = []
    sig_list = []
    for data in data_list:
        mu_list.append(np.mean((data).astype(np.float)))
        sig_list.append(np.std((data).astype(np.float)))
    mu = np.mean(mu_list)
    sig = np.mean(sig_list)
    
    return mu, sig


def get_mean_and_std_from_dataset_v2(data_dict):
    mu_list = []
    sig_list = []
    for data in data_dict.values():
        mu_list.append(np.mean((data).astype(np.float)))
        sig_list.append(np.std((data).astype(np.float)))
    mu = np.mean(mu_list)
    sig = np.mean(sig_list)
    
    return mu, sig