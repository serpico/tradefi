from os import listdir
import os.path
import h5py
import numpy as np


def get_fname_list_from_dir(dir_path, ext='h5'):
    # Get all files in directory:
    dir_fnames = os.listdir(dir_path)
    fname_list = []
    for fname in dir_fnames:
        if fname[0] != '.' and os.path.splitext(fname)[1] == '.' + ext:
            fname_list.append(fname)
    fname_list.sort()

    return fname_list
    
    
def get_dataset_from_keys(path_dataset, key_list, fname_list=None):
    """Imports needed data from data set. Expects data set to be stored as .h5 file.
    
    Args:
        path_dataset (str)
        key_list (list of str): depending on the data set, possible keys are 
            'image', 'mask', 'boxes', 'classes', 'class_map', 'instance_map'
        fname_list (list of str): imports only data whose fname is contained in this list
            if fname_list=None, then imports all files stored at path_dataset
    
    Returns:
        dictionary: key is fileid (i.e. fname), value is another dictionary whose keys are defined by key_list
    """
    if fname_list is None:
        fname_list = get_fname_list_from_dir(path_dataset)
    else:
        for idx, fname in enumerate(fname_list):
            fname_list[idx] = fname + '.h5'
    
    data_dict = {} 
    
    for fname in fname_list:
        fname_wpath = os.path.join(path_dataset, fname)
        
        h5file = h5py.File(fname_wpath, 'r')
        attrib = {}
        for key in key_list:
            attrib[key] = h5file[key][:]
        h5file.close()

        fileid = os.path.splitext(fname)[0] # delete .ext   
        data_dict[fileid] = attrib
        
    return data_dict
    
def write_dataset(path_dataset, dset):
    for fileid, data in dset.items():
        fname_path = os.path.join(path_dataset, fileid+'.h5')
        h5file = h5py.File(fname_path, 'w')
        for key, value in data.items():
            dset = h5file.create_dataset(key, value.shape, dtype=value.dtype)
            dset[:] = value
        h5file.close

def get_dataset_segmentation(path_dataset):
    image_fnames = get_fname_list_from_dir(path_dataset)
    
    data_list = [] 
    targ_list = []
    for fname in image_fnames:
        fname_wpath = os.path.join(path_dataset, fname)
        
        h5file = h5py.File(fname_wpath, 'r')
        image = h5file['image'][:]
        mask = h5file['mask'][:]
        h5file.close()
        
        image = np.float16(image[:,:,2]) # select only blue channel
        mask = np.uint16(mask)
        
        data_list.append(image)
        targ_list.append(mask)
        
    return data_list, targ_list
    
    
def get_dataset_segmentation_v2(path_dataset):
    image_fnames = get_fname_list_from_dir(path_dataset)
    
    data_dict = {} 
    targ_dict = {}
    for fname in image_fnames:
        fname_wpath = os.path.join(path_dataset, fname)
        
        h5file = h5py.File(fname_wpath, 'r')
        image = h5file['image'][:]
        mask = h5file['mask'][:]
        h5file.close()
        
        image = np.float16(image[:,:,2]) # select only blue channel
        mask = np.uint16(mask)
        
        key = os.path.splitext(fname)[0] # delete .ext   
        data_dict[key] = image
        targ_dict[key] = mask
        
    return data_dict, targ_dict
    
    
def get_dataset_autoencoder(path_dataset, sigma_noise=0, sample=None, channel=None):
    image_fnames = get_fname_list_from_dir(path_dataset)
    
    if sample is not None:  # to avoid loading whole dataset for short tests 
        image_fnames = image_fnames[:sample]
    
    data_dict = {} 
    targ_dict = {}
    for fname in image_fnames:
        fname_wpath = os.path.join(path_dataset, fname)
        
        h5file = h5py.File(fname_wpath, 'r')
        image = h5file['image'][:]
        h5file.close()
        
        image = np.float16(image)
        
        if channel is not None:
            image = image[:,:,channel]
        
        key = os.path.splitext(fname)[0] # delete .ext   
        data_dict[key] = image + sigma_noise*np.random.normal(0, 1, image.shape).astype(np.float16)
        targ_dict[key] = image
        
    return data_dict, targ_dict