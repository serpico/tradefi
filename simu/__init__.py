import numpy as np
from sklearn.cluster import KMeans
from skimage.filters import gaussian
from skimage.morphology import binary_dilation, disk

class Simulator:
    def __init__(self, img_blue, imap):
        if img_blue.shape != imap.shape:
            raise Exception('"img" and "imap" are not of same size')

        self.img_blue = img_blue
        self.imap = imap

        # Simulation parameters
        self.mask_dilation_radius = 3
        self.gaussian_sigma = 3  # controls the intensity of blurring in fluo channels
        self.correct_coef = 4  # for correcting dynamic range of R and G channels
        self.chromo_size_thr = 200  # only chromos above this thr are considered for coloring

        # Initialize simu:
        self.img_sim = np.zeros(img_blue.shape + (3,))
        self.img_sim[:, :, 2] = img_blue

        # Annotation parameters:
        self.label_normal = 1
        self.label_color = 2
        self.label_transloc = 3

        # Initialize annotations:
        self.cmap = np.zeros(imap.shape, dtype=np.uint8)
        self.cmap[imap > 0] = self.label_normal  # all normal chromos are class 1

        self.classes = np.ones(len(np.unique(imap))-1, dtype=np.uint8)

    def blue2fluo(self, n_transloc):
        instance_idx_list = self.get_instance_idx_list()

        idx_red = self.sample_without_replacement(instance_idx_list, 2)
        self.get_color_chromo(idx_red, 'red')

        idx_gre = self.sample_without_replacement(instance_idx_list, 2)
        self.get_color_chromo(idx_gre, 'green')

        idx_yel = self.sample_without_replacement(instance_idx_list, 2)
        self.get_color_chromo(idx_yel, 'yellow')

        if n_transloc > 0:
            #idx_tra = self.sample_without_replacement(instance_idx_list, n_transloc)
            #self.get_transloc(idx_tra)
            for _ in range(n_transloc):  # translocs are added 1 by 1 so that each has rnd color
                idx_tra = self.sample_without_replacement(instance_idx_list, 1)
                self.get_transloc(idx_tra)

        return self.img_sim, self.cmap, self.classes

    def get_instance_idx_list(self):
        instance_idx_list = np.unique(self.imap)
        instance_idx_list = np.delete(instance_idx_list, 0)  # delete instance_idx '0' (background)

        # Only keep chromos that are larger than self.chromo_size_thr:
        chromo_size_list = []
        for idx in instance_idx_list:
            chromo_size_list.append(np.sum(self.imap==idx))
        chromo_size_list = np.array(chromo_size_list)

        instance_idx_list = instance_idx_list[chromo_size_list >= self.chromo_size_thr]

        return instance_idx_list.tolist()

    def sample_without_replacement(self, input_list, N):
        """Samples randomly N elements from 1D array, and delete these elements from 1D array"""
        # First, sample without replacement from input list:
        output_list = np.random.choice(input_list, N, replace=False)

        # Next, get the index of sampled elements:
        idx_list = []
        for value in output_list:
            indice = np.nonzero(np.array(input_list) == value)
            idx_list.append(indice[0][0])

        # Now, we have to remove these elements from input_list.
        # np.delete does not work, because works by returning a copy.
        # So we have to use pop
        idx_list = np.flip(np.sort(idx_list))  # sort in descending order, else poping elements is a mess
        for idx in idx_list:
            input_list.pop(idx)

        return output_list.tolist()

        # output_list = []
        # for _ in range(N):
        #     rnd_idx = np.random.randint(0, len(input_list))
        #     output_list.append(input_list[rnd_idx])
        #     input_list = np.delete(input_list, rnd_idx)
        # return output_list

    def get_color_chromo(self, instance_idx, color):
        mask = self.get_mask_chromo(instance_idx)
        self.colorize_chromo(mask, color)

        # Get annotations:
        self.cmap[mask == 1] = self.label_color
        for idx in instance_idx:
            self.classes[idx-1] = self.label_color

    def get_mask_chromo(self, instance_idx):
        """Mask is '1' for all chromo instances selected in instance_idx"""
        mask = np.zeros(self.imap.shape)
        for idx in instance_idx:
            mask += (self.imap == idx)
        return mask

    def colorize_chromo(self, mask, color):
        #mask = gaussian(mask, sigma=2)
        mask = binary_dilation(mask, footprint=disk(radius=self.mask_dilation_radius))
        channel_sim = (self.img_blue * self.correct_coef) * mask
        channel_sim = gaussian(channel_sim, sigma=self.gaussian_sigma)

        if color == 'red':
            self.img_sim[:, :, 0] += channel_sim
            self.img_sim[:, :, 1] += 0
        elif color == 'green':
            self.img_sim[:, :, 0] += 0
            self.img_sim[:, :, 1] += channel_sim
        elif color == 'yellow':
            self.img_sim[:, :, 0] += channel_sim
            self.img_sim[:, :, 1] += channel_sim
        else:
            raise Exception('Color can only be "red", "green", or "yellow"')

    def get_transloc(self, instance_idx):
        mask_chromo = self.get_mask_chromo(instance_idx)
        mask_transloc = self.get_mask_transloc(mask_chromo)
        color = np.random.choice(['red', 'green', 'yellow'])
        self.colorize_chromo(mask_transloc, color)

        # Get annotations:
        self.cmap[mask_chromo == 1] = self.label_transloc
        for idx in instance_idx:
            self.classes[idx-1] = self.label_transloc

    def get_mask_transloc(self, mask_chromo):
        indices = np.nonzero(mask_chromo == 1)
        kmeans = KMeans(n_clusters=3).fit(np.transpose(np.array(indices)))

        mask_transloc = np.zeros(mask_chromo.shape)
        for idx, lbl in enumerate(kmeans.labels_):
            x = indices[0][idx]
            y = indices[1][idx]
            mask_transloc[x, y] = lbl + 1

        cluster_id = 1
        mask_transloc = (mask_transloc == cluster_id)

        return mask_transloc
