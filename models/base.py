from tensorflow.keras.layers import Input, concatenate, Activation
from tensorflow.keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from tensorflow.keras.models import Model


class NetBase:
    def conv_layer(self, layer_input, filters, kernel_size):
        x = Conv2D(filters, kernel_size, padding='same')(layer_input)
        layer_output = Activation('relu')(x)
        return layer_output
        
    def conv_block(self, block_input, filters, kernel_size):
        x = self.conv_layer(block_input, filters, kernel_size)
        block_output = self.conv_layer(x, filters, kernel_size)
        return block_output
        
        
class Encoder(NetBase):
    def get_encoder(self, dim_in, n_channels):
        input = Input(shape=dim_in+(n_channels,))

        stage1 = self.conv_block(input, filters=64, kernel_size=(3,3))
        x = MaxPooling2D((2,2), strides=None)(stage1)
        
        stage2 = self.conv_block(x, filters=128, kernel_size=(3,3))
        x = MaxPooling2D((2,2), strides=None)(stage2)
        
        output = self.conv_block(x, filters=254, kernel_size=(3,3))
        
        return Model(input, output), [stage1, stage2]
        
        
class Unet(Encoder):
    def get_net(self, dim_in, n_channels, n_classes):
        encoder, [stage1, stage2] = self.get_encoder(dim_in, n_channels)
        encoder_in  = encoder.layers[0].input
        encoder_out = encoder.layers[-1].output
        
        x = self.conv_block(encoder_out, filters=254, kernel_size=(3,3))
        
        x = UpSampling2D(size=(2,2), data_format='channels_last')(x)
        x = Conv2D(128, (2,2), padding='same', activation='relu')(x)
        x = concatenate([x, stage2])
        
        x = self.conv_block(x, filters=128, kernel_size=(3,3))
        
        x = UpSampling2D(size=(2,2), data_format='channels_last')(x)
        x = Conv2D(64, (2,2), padding='same', activation='relu')(x)
        x = concatenate([x, stage1])
        
        x = self.conv_block(x, filters=64, kernel_size=(3,3))
        
        output = Conv2D(n_classes, (1,1), padding='same', activation='softmax')(x)
    
        return Model(encoder_in, output)


class AutoEncoder(Encoder):
    def get_net(self, dim_in, n_channels):
        encoder, [stage1, stage2] = self.get_encoder(dim_in, n_channels)
        encoder_in  = encoder.layers[0].input
        encoder_out = encoder.layers[-1].output
        
        x = self.conv_block(encoder_out, filters=254, kernel_size=(3,3))
        
        x = UpSampling2D(size=(2,2), data_format='channels_last')(x)
        x = Conv2D(128, (2,2), padding='same', activation='relu')(x)
        
        x = self.conv_block(x, filters=128, kernel_size=(3,3))
        
        x = UpSampling2D(size=(2,2), data_format='channels_last')(x)
        x = Conv2D(64, (2,2), padding='same', activation='relu')(x)
        
        x = self.conv_block(x, filters=64, kernel_size=(3,3))
        
        output = Conv2D(n_channels, (1,1), padding='same', activation=None)(x)
    
        return Model(encoder_in, output)


# model = Unet().get_net(dim_in=(128,128), n_channels=3, n_classes=2)
# model.summary()
#
# from keras.utils.vis_utils import plot_model
# plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)