# Code from this file is from: https://keras.io/examples/generative/vae/

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from .base import NetBase
from training import DataGenerator


class Encoder(NetBase):
    def __call__(self, dim_in, n_channels, dim_latent):
        encoder_in = keras.Input(shape=dim_in + (n_channels,))

        stage1 = self.conv_block(encoder_in, filters=64, kernel_size=(3, 3))
        x = layers.MaxPooling2D((2, 2), strides=None)(stage1)

        stage2 = self.conv_block(x, filters=128, kernel_size=(3, 3))
        x = layers.MaxPooling2D((2, 2), strides=None)(stage2)

        stage3 = self.conv_block(x, filters=254, kernel_size=(3, 3))

        x = layers.Flatten()(stage3)
        x = layers.Dense(16, activation='relu')(x)

        z_mean = layers.Dense(dim_latent, name="z_mean")(x)
        z_log_var = layers.Dense(dim_latent, name="z_log_var")(x)
        z = Sampling()([z_mean, z_log_var])
        encoder = keras.Model(encoder_in, [z_mean, z_log_var, z], name="encoder")

        return encoder


class Decoder(NetBase):
    def __call__(self, encoder):
        n_channels = encoder.layers[0].input_shape[0][-1]
        dim_latent = encoder.layers[-1].output_shape[1]
        fmap_shape = encoder.layers[-6].output_shape[1:4]  # shape of feat maps before flatten

        decoder_in = keras.Input(shape=(dim_latent,))

        x = layers.Dense(fmap_shape[0]*fmap_shape[1]*fmap_shape[2], activation="relu")(decoder_in)
        x = layers.Reshape(fmap_shape)(x)

        x = self.conv_block(x, filters=254, kernel_size=(3, 3))

        x = layers.UpSampling2D(size=(2, 2), data_format='channels_last')(x)
        x = layers.Conv2D(128, (2, 2), padding='same', activation='relu')(x)

        x = self.conv_block(x, filters=128, kernel_size=(3, 3))

        x = layers.UpSampling2D(size=(2, 2), data_format='channels_last')(x)
        x = layers.Conv2D(64, (2, 2), padding='same', activation='relu')(x)

        x = self.conv_block(x, filters=64, kernel_size=(3, 3))

        decoder_out = layers.Conv2D(n_channels, (1, 1), padding='same', activation='sigmoid')(x)

        decoder = keras.Model(decoder_in, decoder_out, name='decoder')

        return decoder


class Sampling(layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon


class VAE(keras.Model):
    def __init__(self, encoder, decoder, **kwargs):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
        ]

    def train_step(self, data):
        with tf.GradientTape() as tape:
            data = data[0]  # [eml] necessary because my data generator outputs (data, target)
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.binary_crossentropy(data, reconstruction), axis=(1, 2)
                )
            )
            kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
            total_loss = reconstruction_loss + kl_loss
        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        return {
            "loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
        }

    def call(self, data):
        z_mean, z_log_var, z = self.encoder(data)
        reconstructed = self.decoder(z)
        return reconstructed


class DataGeneratorVAE(DataGenerator):
    def __data_generation(self, list_IDs_temp):
        'Generates batches'  # X : (n_samples, *dim, n_channels)
        # Initialization
        batch_data = np.empty((self.batch_size, *self.dim, self.n_channels))

        if self.n_classes is not None:  # if classif
            batch_targ = np.empty((self.batch_size, *self.dim), dtype=int)
        elif self.n_channels > 1:  # if regression
            batch_targ = np.empty((self.batch_size, *self.dim, self.n_channels))
        elif self.n_channels == 1:  # if regression with only 1 channel
            batch_targ = np.empty((self.batch_size, *self.dim))

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            img_shape = self.data_dict[ID].shape

            # Sample patch at random position and store in batch:
            x = np.random.choice(range(0, img_shape[0] - self.dim[0]))
            y = np.random.choice(range(0, img_shape[1] - self.dim[1]))

            patch_data = self.data_dict[ID][x:x + self.dim[0], y:y + self.dim[1]]
            patch_targ = self.targ_dict[ID][x:x + self.dim[0], y:y + self.dim[1]]

            if self.n_channels == 1:
                batch_data[i, :, :, 0] = patch_data
            else:
                batch_data[i, :, :, :] = patch_data

            batch_targ[i, :, :] = patch_targ

        batch_data, batch_targ = self.data_augmentor(batch_data, batch_targ)

        if self.n_classes is not None:
            batch_targ = keras.utils.to_categorical(batch_targ, num_classes=self.n_classes)

        return batch_data