import numpy as np
import tensorflow.keras as keras
import tensorflow as tf

from sklearn.metrics import precision_recall_fscore_support

import matplotlib.pyplot as plt
import utils.tools
import io

# from: https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
class DataGenerator_old(keras.utils.Sequence):  # old version where dset is a list. In new version, dset is a dict
    'Generates data for Keras'
    def __init__(self, data_list, targ_list, data_augmentor, batch_size=32, dim=(32,32), n_channels=1,
                 n_classes=10, shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        #self.labels = labels
        self.list_IDs = list(range(len(data_list)))
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()
        
        self.data_list = data_list
        self.targ_list = targ_list
        self.data_augmentor = data_augmentor

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index): # generates the batch
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        data, targ = self.__data_generation(list_IDs_temp)

        return data, targ

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
        
    def __data_generation(self, list_IDs_temp):
        'Generates batches' # X : (n_samples, *dim, n_channels)
        # Initialization
        batch_data = np.empty((self.batch_size, *self.dim, self.n_channels))
        batch_targ = np.empty((self.batch_size, *self.dim), dtype=int)
        
        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            img_shape = self.data_list[ID].shape
            
            # Sample patch at random position and store in batch:
            x = np.random.choice(range(0, img_shape[0]-self.dim[0]))
            y = np.random.choice(range(0, img_shape[1]-self.dim[1]))
            # l = 350
            # x = np.random.choice(range(0+l, img_shape[0]-self.dim[0]-l))
            # y = np.random.choice(range(0+l, img_shape[1]-self.dim[1]-l))
            
            patch_data = self.data_list[ID][x:x+self.dim[0], y:y+self.dim[1]]
            patch_targ = self.targ_list[ID][x:x+self.dim[0], y:y+self.dim[1]]
            
            if self.n_channels==1:
                batch_data[i,:,:,0] = patch_data
            else:
                 batch_data[i,:,:,:] = patch_data
            
            batch_targ[i,:,:] = patch_targ 
            
        batch_data, batch_targ = self.data_augmentor(batch_data, batch_targ)
        
        return batch_data, keras.utils.to_categorical(batch_targ, num_classes=self.n_classes)


# inspired from: https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
# if n_classes=None, the generator is in regressions setting (eg autoencoder)
class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, data_dict, targ_dict, data_augmentor, batch_size=32, dim=(32,32), n_channels=1,
                 n_classes=None, shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        #self.labels = labels
        self.list_IDs = list(data_dict.keys())
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()
        
        self.data_dict = data_dict
        self.targ_dict = targ_dict
        self.data_augmentor = data_augmentor

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index): # generates the batch
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        data, targ = self.__data_generation(list_IDs_temp)

        return data, targ

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
        
    def __data_generation(self, list_IDs_temp):
        'Generates batches' # X : (n_samples, *dim, n_channels)
        # Initialization
        batch_data = np.empty((self.batch_size, *self.dim, self.n_channels))
        
        if self.n_classes is not None: # if classif
            batch_targ = np.empty((self.batch_size, *self.dim), dtype=int)
        elif self.n_channels > 1: # if regression
            batch_targ = np.empty((self.batch_size, *self.dim, self.n_channels))
        elif self.n_channels == 1: # if regression with only 1 channel
            batch_targ = np.empty((self.batch_size, *self.dim))
        
        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            img_shape = self.data_dict[ID].shape
            
            # Sample patch at random position and store in batch:
            x = np.random.choice(range(0, img_shape[0]-self.dim[0]))
            y = np.random.choice(range(0, img_shape[1]-self.dim[1]))
            
            patch_data = self.data_dict[ID][x:x+self.dim[0], y:y+self.dim[1]]
            patch_targ = self.targ_dict[ID][x:x+self.dim[0], y:y+self.dim[1]]
            
            if self.n_channels==1:
                batch_data[i,:,:,0] = patch_data
            else:
                 batch_data[i,:,:,:] = patch_data
            
            batch_targ[i,:,:] = patch_targ 
            
        batch_data, batch_targ = self.data_augmentor(batch_data, batch_targ)
        
        if self.n_classes is not None:
            batch_targ = keras.utils.to_categorical(batch_targ, num_classes=self.n_classes)
        
        return batch_data, batch_targ


        
           
class DataAugmentor:
    
    def __call__(self, batch_data, batch_targ):
        for idx in range(batch_data.shape[0]):
            batch_data[idx,], batch_targ[idx,] = self.augment(batch_data[idx,], batch_targ[idx,])
        return batch_data, batch_targ
        
    def augment(self, data, targ):
        data = self.norm_0mean_1std(data)
        data, targ = self.random_permute_and_flip(data, targ)
        return data, targ
    
    def norm_0mean_1std(self, data):
        n_channels = data.shape[-1]
        
        if data.ndim==2:
            data = (data - np.mean(data)) / np.std(data)
        elif data.ndim==3:
            for c in range(n_channels):
                data[:,:,c] = (data[:,:,c] - np.mean(data[:,:,c])) / np.std(data[:,:,c])
            
        return data
        
    def norm_offset_scale(self, data, offset, scale):  
        return (data - offset) / scale

    def norm_ab(self, data, a, b):
        return a + (b - a) * (data - np.min(data)) / (np.max(data) - np.min(data))

    def norm_ab_per_channel(self, data, a, b):
        for idx in range(data.shape[-1]):
            data[:, :, idx] = self.norm_ab(data[:, :, idx], a, b)
        return data
        
    def random_permute_and_flip(self, data, targ):
        axes_spatial = [0, 1] 
        perm = tuple(np.random.permutation(axes_spatial))
        data = np.transpose(data, axes=perm + (2,))
        
        if targ.ndim==2: # if classif
            targ = np.transpose(targ, axes=perm)
        elif targ.ndim==3: # if regression
            targ = np.transpose(targ, axes=perm + (2,))
            
        for ax in axes_spatial: 
            if np.random.rand() > 0.5:
                data = np.flip(data, axis=ax)
                targ = np.flip(targ, axis=ax)
        return data, targ
        
    def add_gaussian_noise(self, data): # inspired from stardist
        sig_noise = 0.02 * np.random.uniform(0, 1)
        return data + sig_noise * np.random.normal(0, 1, data.shape)
        
    def random_intensity_change(self, data): # inspired from stardist
        return data * np.random.uniform(0.6, 2) + np.random.uniform(-0.2, 0.2)
        
    def plot_augment_preview(self, data, targ, fname='data_augment_preview.png'):
        fontsize = 10
        fig, axes = plt.subplots(2,6)
        fig.set_size_inches(20,10)
        img = utils.tools.norm_rgb_img_for_plot(data)
        axes[0,0].imshow(img, vmin=0, vmax=1)
        axes[0,0].set_title('Original data', fontsize=fontsize)
        axes[1,0].imshow(targ, vmin=np.min(targ), vmax=np.max(targ))
        axes[1,0].set_title('Original target', fontsize=fontsize)
        
        for idx in range(1,6):
            data_augm, targ_augm = self.augment(data, targ)
            img_augm = utils.tools.norm_rgb_img_for_plot(data_augm)
            axes[0,idx].imshow(img_augm, vmin=0, vmax=1)
            axes[0,idx].set_title(f'Augment {idx} data', fontsize=fontsize)
            axes[1,idx].imshow(targ_augm, vmin=np.min(targ_augm), vmax=np.max(targ_augm))
            axes[1,idx].set_title(f'Augment {idx} target', fontsize=fontsize)
            
        #axes.axis('off')
        fig.savefig(fname)
            
            
            
class ValScoreCallback(tf.keras.callbacks.Callback):

    def __init__(self, log_dir, generator_val, label_list):
        #self.tb_callback = tb_callback
        self.generator_val = generator_val
        self.step_number = 0
        self.label_list = label_list
        self.tb_writer = tf.summary.create_file_writer(log_dir)

    def on_epoch_end(self, epoch, logs=None):

        pre_list = []
        rec_list = []
        f1s_list = []
        for data, targ in self.generator_val:
            pred = self.model.predict(data)
            
            scores = precision_recall_fscore_support(
                targ.argmax(axis=-1).flatten(),
                pred.argmax(axis=-1).flatten(),
                average=None,
                labels=self.label_list
            )
            pre_list.append(scores[0])
            rec_list.append(scores[1])
            f1s_list.append(scores[2])
            
        pre = np.mean(np.array(pre_list), axis=0)
        rec = np.mean(np.array(rec_list), axis=0)
        f1s = np.mean(np.array(f1s_list), axis=0)
            
        
        metric_dict = {}
        for idx, lbl in enumerate(self.label_list):
            metric_dict['precision_class' + str(lbl)] = pre[idx]
            metric_dict['recall_class' + str(lbl)] = rec[idx]
            metric_dict['f1score_class' + str(lbl)] = f1s[idx]
                
        self.write_to_tb(metric_dict)


    def write_to_tb(self, metric_dict):

        for name, value in metric_dict.items():
            with self.tb_writer.as_default():
                tf.summary.scalar(name, value, step=self.step_number)

            self.tb_writer.flush()

        self.step_number += 1
        
        
class PredictionImageCallback(tf.keras.callbacks.Callback):
    
    def __init__(self, log_dir, data_sample, targ_sample):
        self.data_sample = data_sample
        self.targ_sample = targ_sample
        self.tb_writer = tf.summary.create_file_writer(log_dir)
    
    def on_epoch_end(self, epoch, logs=None):
        batch_data = np.expand_dims(self.data_sample, axis=0)

        # Predict:
        pred = self.model.predict(batch_data)

        # Prepare arrays for plot:
        img_pred = ( np.argmax(pred[0], axis=-1) ).astype(dtype=int)
        img_targ = ( self.targ_sample ).astype(dtype=int)
        img_data = ( utils.tools.norm_rgb_img_for_plot(self.data_sample) ).astype(dtype=float)
        
        
        # Plot:
        fontsize = 10
        fig, axes = plt.subplots(1,3)
        fig.set_size_inches(20,10)

        axes[0].imshow(img_data, vmin=0, vmax=1)
        axes[0].set_title('Data', fontsize=fontsize)
        
        axes[1].imshow(img_pred, vmin=np.min(img_pred), vmax=np.max(img_pred))
        axes[1].set_title(f'Prediction, epoch {epoch}', fontsize=fontsize)

        axes[2].imshow(img_targ, vmin=np.min(img_targ), vmax=np.max(img_targ))
        axes[2].set_title('Target', fontsize=fontsize)

        # Write to tensorboard:
        with self.tb_writer.as_default():
            tf.summary.image('Prediction', self.figure2tensor(fig), step=epoch) 
        
    # from: https://neptune.ai/blog/tensorboard-tutorial
    def figure2tensor(self, figure):
        buf = io.BytesIO()
        plt.savefig(buf, format='png', bbox_inches='tight')
        plt.close(figure)
        buf.seek(0)

        digit = tf.image.decode_png(buf.getvalue(), channels=4)
        digit = tf.expand_dims(digit, 0)

        return digit
        
        
class PredictionImageRegressionCallback(PredictionImageCallback):
    def on_epoch_end(self, epoch, logs=None):
        batch_data = np.expand_dims(self.data_sample, axis=0)

        # Predict:
        pred = self.model.predict(batch_data)

        # Prepare arrays for plot:
        img_pred = ( utils.tools.norm_rgb_img_for_plot(pred[0]) ).astype(dtype=float)
        img_targ = ( utils.tools.norm_rgb_img_for_plot(self.targ_sample) ).astype(dtype=float)
        img_data = ( utils.tools.norm_rgb_img_for_plot(self.data_sample) ).astype(dtype=float)
        
        
        # Plot:
        fontsize = 10
        fig, axes = plt.subplots(1,3)
        fig.set_size_inches(20,10)

        axes[0].imshow(img_data)
        axes[0].set_title('Data', fontsize=fontsize)
        
        axes[1].imshow(img_pred)
        axes[1].set_title(f'Prediction, epoch {epoch}', fontsize=fontsize)

        axes[2].imshow(img_targ)
        axes[2].set_title('Target', fontsize=fontsize)

        # Write to tensorboard:
        with self.tb_writer.as_default():
            tf.summary.image('Prediction', self.figure2tensor(fig), step=epoch)
        
    # # from: https://martin-mundt.com/tensorboard-figures/
#     def plot_to_tensorboard(self, writer, fig, step):
#         """
#         Takes a matplotlib figure handle and converts it using
#         canvas and string-casts to a numpy array that can be
#         visualized in TensorBoard using the add_image function
#
#         Parameters:
#             writer (tensorboard.SummaryWriter): TensorBoard SummaryWriter instance.
#             fig (matplotlib.pyplot.fig): Matplotlib figure handle.
#             step (int): counter usually specifying steps/epochs/time.
#         """
#
#         # Draw figure on canvas
#         fig.canvas.draw()
#
#         # Convert the figure to numpy array, read the pixel values and reshape the array
#         img = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
#         img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
#
#         # Normalize into 0-1 range for TensorBoard(X). Swap axes for newer versions where API expects colors in first dim
#         img = img / 255.0
#         # img = np.swapaxes(img, 0, 2) # if your TensorFlow + TensorBoard version are >= 1.8
#
#         # Add figure in numpy "image" to TensorBoard writer
#         writer.add_image('Prediction', img, step)
#         plt.close(fig)
    
    # # from https://stackoverflow.com/questions/38543850/how-to-display-custom-images-in-tensorboard-e-g-matplotlib-plots
    # def fig2rgb_array(self, fig, expand=True):
    #     fig.canvas.draw()
    #     buf = fig.canvas.tostring_rgb()
    #     ncols, nrows = fig.canvas.get_width_height()
    #     shape = (nrows, ncols, 3) if not expand else (1, nrows, ncols, 3)
    #     return np.fromstring(buf, dtype=np.uint8).reshape(shape)
        